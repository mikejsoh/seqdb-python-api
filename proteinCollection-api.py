import requests,json,argparse,pprint
parser = argparse.ArgumentParser()
parser.add_argument("--post", help='POST method', nargs=3, metavar=('"protein name"','groupId', '"protein description"'))
parser.add_argument("--get", help="GET Method", nargs=1, metavar=('id'))
parser.add_argument("--put", help='PUT method', nargs=4, metavar=('id','"description"','groupid','"region name"'))
parser.add_argument("--delete", help="Delete method", nargs=1, metavar=('id'))

args = parser.parse_args()


headers = {'apikey': 'adminkey', 'Content-Type':'application/json','Accept': 'application/json'}
url = 'http://localhost:10000/api/v1/proteinCollection/'
id = 0

if args.post: #create
   data = { "proteinCollection": {"description":args.post[0],"group":{"id": args.post[1]}, "name":args.post[2] } }
   r = requests.post(url, data=json.dumps(data), headers=headers)
   pprint.pprint(r.json())

if args.get: #list 
   id = args.get[0]
   r = requests.get(url + str(id), headers=headers)
   pprint.pprint(r.json())

if args.put: #update
   id = args.put[0]
   data = { 'proteinCollection': {'description':args.put[3],'group':{'id':args.put[2]}, 'name':args.put[1]  } }
   data_json = json.dumps(data) # converts data into valid json format
   r = requests.put(url + str(id), data=data_json, headers=headers)
   pprint.pprint(r.json())

if args.delete: #delete
   id = args.delete[0]
   r = requests.delete(url + str(id), headers=headers)
   pprint.pprint(r.json())