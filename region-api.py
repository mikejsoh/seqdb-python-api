import requests,json,argparse,pprint
parser = argparse.ArgumentParser()
parser.add_argument("--post", help='POST method api_region_req.py --post <"description"> <group id> <"name">', nargs=3, metavar=('"region description"','groupId', '"region name"'))
parser.add_argument("--get", help="GET Method", nargs=1, metavar=('id'))
parser.add_argument("--put", help='PUT method', nargs=4, metavar=('id','"description"','groupid','"region name"'))
parser.add_argument("--delete", help="Delete method", nargs=1, metavar=('id'))

args = parser.parse_args()


headers = {'apikey': 'adminkey', 'Content-Type':'application/json','Accept': 'application/json'}
url = 'http://localhost:10000/api/v1/region/'
id = 0

if args.post: #create
   data = { "region": {"description":args.post[0],"group":{"id": args.post[1]}, "name":args.post[2] } }
   r = requests.post(url, data=json.dumps(data), headers=headers)
   pprint.pprint(r.json())

if args.get: #list 
   id = args.get[0]
   r = requests.get('http://localhost:10000/api/v1/region/' + str(id), headers=headers)
   pprint.pprint(r.json(), indent=4)

if args.put: #update
   id = args.put[0]
   data = { 'region': {'description':args.put[1],'group':{'id':args.put[2]}, 'name':args.put[3]  } }
   data_json = json.dumps(data) # converts data into valid json format
   r = requests.put('http://localhost:10000/api/v1/region/' + str(id), data=data_json, headers=headers)
   pprint.pprint(r.json())

if args.delete: #delete
   id = args.delete[0]
   r = requests.delete('http://localhost:10000/api/v1/region/' + str(id), headers=headers)
   pprint.pprint(r.json())


#curl -g -X PUT -H "apikey: adminkey" -H "Content-Type: application/json" -H "Accept: application/json" -d '{"region":{"description":"Update Gene Region", "group": "1", "name":"Update Gene"}}' http://localhost:10000/api/v1/region/15


#curl -H "apikey: adminkey" http://localhost:10000/api/v1/proteinCollection/1
#curl -X POST -H "apikey: adminkey" -H "Content-Type: application/json" -d '{"proteinCollection" : { "name": "API TEST", "group":{"id": 1} } }' http://localhost:10000/api/v1/proteinCollection/